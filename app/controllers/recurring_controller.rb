class RecurringController < ApplicationController
  include ShopifyApp::Authenticated
  before_action :set_shop
  skip_before_action :verify_authenticity_token#, if: :jwt_shopify_domain
  after_action :set_currency

  def create_recurring_application_charge
    # ShopifyAPI::Shop.current
    if !@shop.charge_id || @shop.app_uninstalled
      recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.new(
        name: "TrueCustomer",
        price: 5,
        return_url: "#{ENV['BASE_APP_URL']}/activatecharge",
        test: true
      )
      if recurring_application_charge.save
        debugger
        fullpage_redirect_to recurring_application_charge.confirmation_url 
      end
    else
      redirect_to dashboard_path
    end
  end

  def activate_charge
    recurring_application_charge = ShopifyAPI::RecurringApplicationCharge.find(request.params['charge_id'])
    if recurring_application_charge.status == 'active'
      trial_end_time = recurring_application_charge.trial_ends_on.to_time
      billing_on = recurring_application_charge.billing_on.to_date + 30.days
      @shop.update(charge_id: recurring_application_charge.id, trial_ends_on: trial_end_time, billing_on: billing_on,
                   charge_cancelled: false, app_uninstalled: false)

      redirect_to dashboard_path
    end
    shopify_shop = ShopifyAPI::Shop.current
    currency_code = shopify_shop.currency
    @shop.update(currency_code: currency_code)
  end

  private

  def set_shop
    @shop = Shop.find_by_shopify_domain(current_shopify_domain)
  end

  def set_currency
    code = @shop.currency_code
    sym = Currency.where(code: code).pluck(:symbol) 
    $sym = sym[0]
  end

end
