# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_26_162418) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "currencies", force: :cascade do |t|
    t.string "code"
    t.string "symbol"
  end

  create_table "customers", id: :bigint, default: nil, force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.string "phone"
    t.integer "orders_count", default: 0
    t.float "total_spent", default: 0.0
    t.json "default_address"
    t.float "rate", default: 4.5
    t.string "newest_updated_at"
    t.string "fulfil_count", default: ["0", "0"], array: true
    t.integer "unfulfil_count", default: 0
    t.integer "cod_count", default: 0
    t.float "avg_count", default: 0.0
    t.string "cities", default: [], array: true
    t.string "zips", default: [], array: true
    t.string "cancel_values", default: ["0", "0"], array: true
    t.json "last_cancel_order", default: {}
    t.bigint "shop_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["shop_id"], name: "index_customers_on_shop_id"
  end

  create_table "orders", id: :bigint, default: nil, force: :cascade do |t|
    t.text "reason_for_cancel"
    t.string "name"
    t.string "customer_name"
    t.bigint "items"
    t.float "total"
    t.string "payment"
    t.string "create_date"
    t.string "cancel_date"
    t.string "email"
    t.string "phone"
    t.string "city"
    t.string "zip"
    t.boolean "status"
    t.string "fulfillment_status"
    t.string "shipping_title"
    t.integer "score", default: 40
    t.string "order_updated_at"
    t.bigint "shop_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "customer_id"
    t.index ["customer_id"], name: "index_orders_on_customer_id"
    t.index ["shop_id"], name: "index_orders_on_shop_id"
  end

  create_table "shops", force: :cascade do |t|
    t.string "shopify_domain", null: false
    t.string "shopify_token", null: false
    t.bigint "charge_id"
    t.boolean "charge_cancelled", default: false
    t.datetime "trial_ends_on"
    t.datetime "billing_on"
    t.json "shopify_json"
    t.boolean "app_uninstalled", default: false
    t.string "currency_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "access_scopes"
    t.index ["shopify_domain"], name: "index_shops_on_shopify_domain", unique: true
  end

  create_table "sync_statuses", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "customers", "shops"
  add_foreign_key "orders", "customers"
  add_foreign_key "orders", "shops"
end
