class CreateSyncStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :sync_statuses do |t|

      t.timestamps
    end
  end
end
