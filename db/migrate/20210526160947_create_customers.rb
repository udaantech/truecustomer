class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers, id: :bigint, default: nil do |t|
      t.string :email
      t.string :name
      t.string :phone
      t.integer :orders_count, default: 0
      t.float :total_spent, default: 0.0
      t.json :default_address
      t.float :rate, default: 4.5
      t.string :newest_updated_at
      t.string :fulfil_count, default: ["0", "0"], array: true
      t.integer :unfulfil_count, default: 0
      t.integer :cod_count, default: 0
      t.float :avg_count, default: 0.0
      t.string :cities, default: [], array: true
      t.string :zips, default: [], array: true
      t.string :cancel_values, default: ["0", "0"], array: true
      t.json :last_cancel_order, default: {}
      t.references :shop, foreign_key: true
            
      t.timestamps
    end
  end
end
