class CreateShops < ActiveRecord::Migration[6.0]
  def self.up
    create_table :shops  do |t|
      t.string :shopify_domain, null: false
      t.string :shopify_token, null: false
      t.bigint :charge_id
      t.boolean :charge_cancelled, default: false
      t.datetime :trial_ends_on
      t.datetime :billing_on
      t.json :shopify_json
      t.boolean :app_uninstalled, default: false
      t.string :currency_code
      t.timestamps
    end

    add_index :shops, :shopify_domain, unique: true
  end

  def self.down
    drop_table :shops
  end
end
