class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders, id: :bigint, default: nil  do |t|
      t.text :reason_for_cancel
      t.string :name
      t.string :customer_name
      t.bigint :items
      t.float  :total
      t.string :payment
      t.string :create_date
      t.string :cancel_date
      t.string :email
      t.string :phone
      t.string :city
      t.string :zip
      t.boolean :status
      t.string :fulfillment_status
      t.string :shipping_title
      t.integer :score, default: 40
      t.string :order_updated_at
      t.references :shop, foreign_key: true

      t.timestamps
    end
  end
end

