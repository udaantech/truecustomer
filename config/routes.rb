Rails.application.routes.draw do
  root :to => 'recurring#create_recurring_application_charge'
  get '/home', to: 'recurring#create_recurring_application_charge', as: :home
  get '/products', :to => 'products#index'
  # get '/charge', to: 'recurring#create_recurring_application_charge', as: :home
  mount ShopifyApp::Engine, at: '/'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
